from django.db import models


# Create your models here.

class Persona(models.Model):
    identificacion = models.CharField(max_length=40)
    nombres = models.CharField(max_length=40)
    apellidos = models.CharField(max_length=40)
    SEXOS = (('F', 'Femenino'), ('M', 'Masculino'))
    Sexo = models.CharField(max_length=1, choices=SEXOS, default='M')
    fechaNacimiento = models.DateField()

    def nombreCompleto(self):
        cadena = "{0} {1}, {2}"
        return cadena.format(self.apellidos, self.nombres, self.identificacion)

    def __str__(self):
        return self.nombreCompleto()


class Recurso(models.Model):
    codigo = models.CharField(max_length=40)
    nombre = models.CharField(max_length=40)
    categoria = models.CharField(max_length=40)
    marca = models.CharField(max_length=40)
    serie = models.CharField(max_length=40)

    def __str__(self):
        return "{0} {1}, {2} {3} {4}".format(self.codigo, self.nombre, self.categoria, self.marca, self.serie)


class Asignacion(models.Model):
    Persona = models.ForeignKey(Persona, null=False, blank=False, on_delete=models.CASCADE)
    Recurso = models.ForeignKey(Recurso, null=False, blank=False, on_delete=models.CASCADE)
    FechaAsignacion = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        cadena = "{0} => {1}"
        return cadena.format(self.Persona, self.Recurso.nombre)
